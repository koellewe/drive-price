package com.sps.kilorand;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

/**
 * Created by Jans Rautenbach on 2015/09/14.
 */

public class JanTools extends AppCompatActivity {

    static public void setContext(Context context){
        ctx = (Activity) context;
    }

    static public Context ctx;

    static public void makeToast(String s){
        if (s.length() < 2){
            Toast.makeText(ctx, s, Toast.LENGTH_SHORT).show();
        }else if (s.substring(s.length() - 2).equals("/l")){
            Toast.makeText(ctx, s.substring(0, s.length() - 2), Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(ctx, s, Toast.LENGTH_SHORT).show();
        }
    }

    public void gotoActivity(Class where){
        Intent intent = new Intent(ctx, where);
        ctx.startActivity(intent);
        finish();
    }

    static public void write(String key, String val){
        SharedPreferences sharedPref = ctx.getSharedPreferences("com.sps.kilorand.info", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, val);
        editor.commit();
    }

    static public String read(String key) {
        SharedPreferences sharedPref = ctx.getSharedPreferences("com.sps.kilorand.info", MODE_PRIVATE);
        return sharedPref.getString(key, "0");
    }

    static public boolean readCmp(String in, String cmp){
        return read(in).equals(cmp);
    }

    static public boolean readBool(String in){
        return read(in).equals("true");
    }

    static public void setDefaultValue(String key, String value){
        if (readCmp(key, "0")){
            write(key, value);
        }
    }

    static public boolean checkConnection(){
        ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public static float convertPixelsToDp(float px){
        Resources resources = ctx.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return px / (metrics.densityDpi / 160f);
    }

    public static int getRandomInt(int min, int max){
        Random rand = new Random();
        return rand.nextInt((max - min) + 1) + min;		//plagiarism lvl: over 9k
    }


}


