package com.sps.kilorand;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.Toolbar;

import com.startapp.android.publish.StartAppSDK;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class MainScreen extends AppCompatActivity {

    public static class MyValues {
        static String appID = "209287282";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StartAppSDK.init(this, MyValues.appID, true);   //last param false to disable return ads
        setContentView(R.layout.activity_main_screen);

        JanTools.setContext(this);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            float exchFromLoading = extras.getFloat("exch", -1);
            if (exchFromLoading != -1) {
                avgTaken = extras.getBoolean("avgTaken");
                transferGasPrice = extras.getDouble("transferGasPrice");
                convertInt = extras.getInt("convertInt");

                if (JanTools.readCmp("conversionType", "money")){
                    conversionType = ConversionType.CURRENCY;
                }else{
                    conversionType = ConversionType.DISTANCE;
                }

                whenExchGotten(exchFromLoading);

            }else{
                JanTools.makeToast(getResources().getString(R.string.error));
            }
        }


    }
    public static DistanceUnit units;
    public static String currency;
    public static float fuelEfficiency;
    public static FuelType fuelType;
    public static String country;
    public static boolean avgTaken = false;
    public static ConversionType conversionType;
    public static int convertInt;

    @Override
    public void onStart() {
        super.onStart();

        setDefaultValues();

        if (!JanTools.readBool("configDone")){
            new JanTools().gotoActivity(Settings.class);
            finish();
        }
    }

    public void setDefaultValues() {
        JanTools.setDefaultValue("metric", "true");
        JanTools.setDefaultValue("diesel", "false");
        JanTools.setDefaultValue("efficiency", "");
        JanTools.setDefaultValue("currency", "USD");
        JanTools.setDefaultValue("country", "Afghanistan");
        JanTools.setDefaultValue("configDone", "false");
    }

    public void onUnitsClick(View view) {
        if (((ToggleButton) view).isChecked()) {
            JanTools.write("metric", "true");
            units = DistanceUnit.METRIC;
            ((TextView) findViewById(R.id.txtConsumption)).setText(getResources().getString(R.string.consumption_metric));
        } else {
            JanTools.write("metric", "false");
            units = DistanceUnit.IMPERIAL;
            ((TextView) findViewById(R.id.txtConsumption)).setText(getResources().getString(R.string.consumption_gay));
        }
    }

    public void onConvertClick(View view) {

        //transferView = view;

        ((TextView) findViewById(R.id.txtResult1)).setText(""); //hide prev result
        ((TextView) findViewById(R.id.txtResult2)).setText("");
        ((TextView) findViewById(R.id.txtResult3)).setText("");
        ((TextView) findViewById(R.id.txtOtherInfo)).setText("");

        //put input where it belongs
        if (!JanTools.checkConnection()) {
            JanTools.makeToast(getResources().getString(R.string.noInternet));
            return;
        }

        if (putPublicValues(true)){
            return;
        }

        if (view == findViewById(R.id.btnConvertDistance)){
            JanTools.write("conversionType", "distance");
        }else{
            JanTools.write("conversionType", "money");
        }

        startActivity(new Intent(this, LoadingView.class));
        finish();
    }

    //public static View transferView;
    public double transferGasPrice;    //not pretty, but it works

    public boolean putPublicValues(boolean error){
        String disRaw = ((EditText) findViewById(R.id.edtConvert)).getText().toString();
        if (disRaw.length() == 0) {
            if (error) {
                JanTools.makeToast(getResources().getString(R.string.giveAValue));
            }
            return true;
        }else{
            convertInt = Integer.parseInt(disRaw);
        }
        if (JanTools.readBool("metric")) {
            units = DistanceUnit.METRIC;
        } else {
            units = DistanceUnit.IMPERIAL;
        }
        country = JanTools.read("country");
        if (JanTools.readBool("diesel")) {
            fuelType = FuelType.DIESEL;
        } else {
            fuelType = FuelType.GASOLINE;
        }
        fuelEfficiency = Float.parseFloat(JanTools.read("efficiency"));
        currency = JanTools.read("currency");

        return false;
    }

    public void whenExchGotten(final Float exch) {

        if (exch == -1f) {
            JanTools.makeToast(getResources().getString(R.string.error));
            System.out.println("exch error");
            finish();
            return;
        }

        putPublicValues(false);

        if (JanTools.readCmp("conversionType", "distance")){
            conversionType = ConversionType.DISTANCE;
        }else{
            conversionType = ConversionType.CURRENCY;
        }

        double gasPrice = transferGasPrice;

        TextView result1 = (TextView) findViewById(R.id.txtResult1);
        TextView result2 = (TextView) findViewById(R.id.txtResult2);
        TextView result3 = (TextView) findViewById(R.id.txtResult3);

        int input = convertInt;

        float efficiency;
        String unitShort;
        if (units == DistanceUnit.IMPERIAL) {
            efficiency = fuelEfficiency * 2.35214583f;  // 1 gallon per 100 mi = 2.35214583 liters per 100 km
            unitShort = "mi";
        } else {
            efficiency = fuelEfficiency;
            unitShort = "km";
        }

                /*
                 *
                 * float efficiency = liters per 100 km (already converted)
                 * float exch = amoutn of user's currency needed to buy 1 USD
                 * double gasPrice = USD per liter of user's fuel type
                 * int input = either distance or money; checked down here
                 *      So either user wants to know how much the distance will cost
                 *      or how far the money will get him
                 *
                 */

        if (conversionType == ConversionType.DISTANCE) {

            double liter = input / 100d * efficiency;    //liters for the distance

            double price = liter * gasPrice;    //USD price for the distance

            double convertedPrice = price * exch;   //convert to user currency

            convertedPrice = Math.round(convertedPrice * 100d) / 100d;

            String space = getResources().getString(R.string.space);

            result1.setText(input + space + unitShort);
            result2.setText(getResources().getString(R.string.willCostYou));
            result3.setText(convertedPrice + space + currency);

        } else {

            double convertedMoney = input / exch;   //converted to USD

            double liter = convertedMoney / gasPrice;

            double distance = liter / efficiency * 100d;     //distance in km

            if (units == DistanceUnit.IMPERIAL) {
                distance = distance * 0.621372292f;      //convert back to mi of necessary
            }

            distance = Math.round(distance * 100d) / 100d;

            String space = getResources().getString(R.string.space);

            result1.setText(input + space + currency);
            result2.setText(getResources().getString(R.string.willGetYou));
            result3.setText(distance + space + unitShort);

        }

        LoadingView loading = new LoadingView();

        loading.finish();

        if (!currency.equals("USD")) {
            gasPrice = gasPrice * exch;
            gasPrice = Math.round(gasPrice * 100d) / 100d;
        }

        String otherInfo = getResources().getString(R.string.otherInfo) + "\n"
                + getResources().getString(R.string.gasPrice) + " = " + gasPrice + "\n";

        if (!currency.equals("USD")) {
            otherInfo = otherInfo.replace("USD", currency);
        }

        otherInfo += getResources().getString(R.string.exchangeRate) + " = " + exch;

        if (avgTaken) {
            otherInfo += "\n\n" + getResources().getString(R.string.avgTaken);
        }

        ((TextView) findViewById(R.id.txtOtherInfo)).setText(otherInfo);

    }

    public void onSettingsClick(View view){
        new JanTools().gotoActivity(Settings.class);
        finish();
    }

    public enum DistanceUnit {
        METRIC,
        IMPERIAL
    }

    public enum FuelType {
        DIESEL,
        GASOLINE
    }

    public enum ConversionType {
        DISTANCE,
        CURRENCY
    }
}
